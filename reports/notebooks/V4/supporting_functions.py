from typing import Union, List
import pandas as pd
import numpy as np
from itertools import groupby
from symbolx import Symbol
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from scipy.interpolate import LinearNDInterpolator
import matplotlib.pyplot as plt
# from plotly_resampler import register_plotly_resampler

# register_plotly_resampler(mode='auto')

def group_and_extract(keys:List[str], reference_key:Union[str,None]=None, sustract: float=0.0, add: float=0.0) -> List[tuple]:
    keys = keys[:]
    if reference_key is not None:
        keys.remove(reference_key)
    
    sorted_list = sorted(keys, key=lambda x: float(x.split('-')[0]), reverse=True)
    
    grouped = groupby(sorted_list, key=lambda x: x.split('-')[0])
    
    result = []
    for key, group in grouped:
        group_list = list(group)
        result.append((key, sorted_list.index(group_list[0]) - sustract, sorted_list.index(group_list[-1]) + add))
    
    return result

def heatmap_mix_plot(symbol, 
                xlabel:str, ylabel:str, 
                xtab:str, ytab:str, 
                legend_title:str="Additional<br>Costs [€/bev]", 
                dtick:int=20, 
                contours_coloring:str="heatmap",
                colorscale:str="armyrose",
                reversescale:bool=False,
                contours_start:float=-250,
                contours_end:float=250,
                contours_size:int=50,
                contours_showlabels:bool=True,
                width:int=800,
                height:int=800,
                secondary_xaxis:bool=False,
                secondary_xtab:str="Flexible [%]",
                ):
    '''
    Generates a heatmap
    '''
    
    x = symbol.dfc[xlabel].values*100
    y = symbol.dfc[ylabel].values*100
    z = symbol.dfc['value'].values
    X0 = np.linspace(0, 100, 101)
    Y0 = np.linspace(0, 100, 101)
    XX, YY = np.meshgrid(X0, Y0)  # 2D grid for interpolation
    interp = LinearNDInterpolator(list(zip(x, y)), z)
    ZZ = interp(XX, YY)


    fig = go.Figure()
    fig.add_traces(go.Contour(
            z=ZZ,
            dx=1,
            x0=0,
            dy=1,
            y0=0,
            showlegend=False,
            showscale=True,
            opacity=1,
            hovertemplate=xtab+": %{x}<br>"+
                          ytab+": %{y}<br>"+
                          "Value: %{z}"+
                          "<extra></extra>",
            contours_coloring=contours_coloring,
            colorscale=colorscale,
            reversescale=reversescale,
            xaxis="x",
            text=[str(x2) for x2 in X0[::-1].tolist()],
            colorbar=dict(
                title=legend_title,
                titleside="top",
                ),
            contours=dict(
                start=contours_start,
                end=contours_end,
                size=contours_size,
                showlabels = contours_showlabels,
            ),
        ))

    fig.add_traces(go.Scatter(
            mode='markers',
            hoverinfo='skip',
            x=x,
            y=y,
            xaxis="x",
            showlegend=False,
            marker=dict(color='black')
        ))
    if secondary_xaxis:
        fig.add_traces(go.Scatter(
                mode='markers',
                hoverinfo='skip',
                x=[0,100],
                y=[0,100],
                xaxis="x2",
                showlegend=False,
                marker=dict(color='black', opacity=0)
            ))
        
    fig.update_layout(
                    autosize=False,
                    width=width,
                    height=height,
                    )
    fig.update_layout(xaxis= {'title':xtab,'showgrid':True, 'side': 'top',})
    fig.update_layout(yaxis= {'title':ytab,'showgrid':True,})
    if secondary_xaxis:
        fig.update_layout(xaxis2= {'title':secondary_xtab, 'showgrid':True,
                                    'anchor': 'y', 
                                    'overlaying': 'x', 
                                    'side': 'bottom',
                                    'tickvals':X0[::dtick],
                                    'ticktext':X0[::-dtick].round(2)
                                    })
    return fig


def update_map(map_dict: dict, **kwargs):
    '''
    Create a master map that contains custom keys given by the kwargs
    Args:
        map (dict): dictionary that map the tech from data as keys to the new tech names.
        kwargs (dict[str,dict]): keys are the category handed over to the new dictionary. It contains dictioanries with that maps original tech names.

    Return:
        dict: keys from the input dictionary with updated maps  
    '''
    mapping = dict()
    mapping['map'] = map_dict
    mapping.update({k: {map_dict[key]: value for key, value in v.items()} for k, v in kwargs.items()})
    return mapping


def update_order(map_dict: dict, **kwargs):
    mapping = dict()
    mapping.update({k: [map_dict[k][elem] if k in map_dict else elem for elem in v] for k, v in kwargs.items()})
    return mapping


def filter_remaining(be_present: list, order: Union[list, dict]) -> Union[list, dict]:
    if isinstance(order, list):
        return [elem for elem in order if elem in be_present]
    elif isinstance(order, dict):
        return {elem: value for elem, value in order.items() if elem in be_present}
    else:
        raise Exception("order argument must be list or dictionary")

def categorize_and_sort(df: pd.DataFrame, category_name: str, scenario_name: str, category_order: Union[list, None] = None, scenario_order: Union[list, None] = None):
    to_be_sorted = []
    if category_order is not None:
        df[category_name] = pd.Categorical(df[category_name], ordered=True, categories=category_order)
        to_be_sorted.append(category_name)
    if scenario_order is not None:
        df[scenario_name] = pd.Categorical(df[scenario_name], ordered=True, categories=scenario_order)
        to_be_sorted.append(scenario_name)
    if to_be_sorted:
        df.sort_values(to_be_sorted, inplace=True)
    return df

def ref_diff_plot(symbol: Symbol, 
                scenario_name: str, 
                category_name: str, 
                criteria_dict: dict,
                criteria_ref_dict: dict,
                category_color_map: Union[dict,None] = None, 
                horizontal_spacing: float = 0.2,
                vertical_spacing: float = 0.05,
                column_widths: Union[List[float],None]=None,
                rounding: int = 2,
                text_template: str = "%{y:.2f}",
                hover_template: str = '%{y:.2f}',
                scenario_order: Union[list, None] = None,
                category_order: Union[list, None] = None,
                facet_col: Union[str, None] = None,
                ) -> go.Figure:

    dfs = []
    df = symbol.shrink_by_attr(neg=False, **criteria_dict, **criteria_ref_dict).round(rounding).dfc
    df = categorize_and_sort(df, category_name, scenario_name, category_order=category_order, scenario_order=scenario_order)

    dfs.append(df)

    fig1 = px.bar(data_frame=df,
                    x=scenario_name,
                    y='value',
                    color=category_name,
                    color_discrete_map=category_color_map,
                    text='value',
                    )
    fig1.update_traces(showlegend=True, textangle=0, textposition='inside', texttemplate=text_template, hovertemplate=hover_template)



    df = symbol.refdiff_by_sections(criteria_dict, criteria_ref_dict).shrink_by_attr(neg=True, **criteria_ref_dict).round(rounding).dfc
    df = categorize_and_sort(df, category_name, scenario_name, category_order=category_order, scenario_order=scenario_order)

    dfs.append(df)

    fig2 = px.bar(data_frame=df,
                    x=scenario_name,
                    y='value',
                    color=category_name,
                    color_discrete_map=category_color_map,
                    text='value',
                    facet_col=facet_col,
                    )
    fig2.update_traces(showlegend=False, textangle=0, textposition='inside', texttemplate=text_template, hovertemplate=hover_template)

    subfig = make_subplots(rows=1, 
                           cols=2, 
                           column_widths=column_widths, 
                           horizontal_spacing=horizontal_spacing, 
                           vertical_spacing=vertical_spacing)
    
    
    [subfig.add_trace(trace, secondary_y=False, row=1, col=2) for trace in fig2['data']]

    subfig.add_vrect(x0="BEV Flex", x1="BEV Inflex", annotation_text="BEV", annotation_position="top left", fillcolor="green", opacity=0.25, line_width=0, layer='below')
    subfig.add_vrect(x0="ERS-BEV Flex", x1="ERS-BEV Inflex", annotation_text="ERS-BEV", annotation_position="top left", fillcolor="pink", opacity=0.25, line_width=0, layer='below')
    subfig.add_vrect(x0="FCEV Centralized", x1="ICEV PtL", annotation_text="H<sub>2</sub>", annotation_position="top left", fillcolor="blue", opacity=0.25, line_width=0, layer='below')
    
    [subfig.add_trace(trace, secondary_y=False, row=1, col=1) for trace in fig1['data']]    
    return subfig, dfs


def refanddiff(var:Symbol, 
               criteria_dict, criteria_ref_dict, 
               category, scenario, 
               category_color, category_order, 
               scenario_order, 
               text_template, hover_template, 
               pattern_shape=None,
               dfs=[]):

    df = var.shrink_by_attr(neg=False, **criteria_dict, **criteria_ref_dict).round(2).dfc
    df = categorize_and_sort(df, category, scenario, category_order=category_order, scenario_order=scenario_order)

    dfs.append(df)

    ref_fig = px.bar(data_frame=df, x=scenario, y='value', color=category, color_discrete_map=category_color, text='value', pattern_shape=pattern_shape)
    ref_fig.update_traces(showlegend=False, textangle=0, textposition='inside', texttemplate=text_template, hovertemplate=hover_template)

    df = var.refdiff_by_sections(criteria_dict, criteria_ref_dict).shrink_by_attr(neg=True, **criteria_ref_dict).round(2).dfc
    df = categorize_and_sort(df, category, scenario, category_order=category_order, scenario_order=scenario_order)

    dfs.append(df)

    diff_fig = px.bar(data_frame=df, x=scenario, y='value', color=category, color_discrete_map=category_color, text='value', pattern_shape=pattern_shape)
    diff_fig.update_traces(showlegend=False, textangle=0, textposition='inside', texttemplate=text_template, hovertemplate=hover_template)

    return ref_fig, diff_fig

def update_map(map_dict: dict, **kwargs):
    '''
    Create a master map that contains custom keys given by the kwargs
    Args:
        map (dict): dictionary that map the tech from data as keys to the new tech names.
        kwargs (dict[str,dict]): keys are the category handed over to the new dictionary. It contains dictioanries with that maps original tech names.

    Return:
        dict: keys from the input dictionary with updated maps  
    '''
    mapping = dict()
    mapping['map'] = map_dict
    mapping.update({k:{map_dict[key]: value for key, value in v.items()} for k,v in kwargs.items()})
    return mapping

def plot(cases, symbol, custom_dict, criteria_dict, criteria_ref_dict, scenario, category, ref_key, category_color, category_order, scenario_order, text_template, hover_template, annotation, annotation_sustract, annotation_add, pattern_shape=None):
    length = len(cases)
    dfs = []
    ref_figs = []
    diff_figs = []
    for criteria in cases:
        var = symbol.query(criteria)
        var.metadata.update(**custom_dict)

        ref_fig, diff_fig = refanddiff(var, criteria_dict, criteria_ref_dict, category, scenario, category_color, category_order, scenario_order, text_template, hover_template, pattern_shape=pattern_shape, dfs=dfs)

        ref_figs.append(ref_fig)
        diff_figs.append(diff_fig)

    subfig = make_subplots(rows=length, cols=2, column_widths=[0.03, 0.6], horizontal_spacing=0.07, vertical_spacing=0.025, shared_yaxes=False, shared_xaxes=True)

    for i, fig in enumerate(diff_figs):
        for trace in fig['data']:
            subfig.add_trace(trace, secondary_y=False, row=i+1, col=2)

    for name, start, end in group_and_extract(scenario_order, reference_key=ref_key, sustract=annotation_sustract, add=annotation_add):
        subfig.add_vrect(x0=start, x1=end, annotation_text=annotation.format(name=name), annotation_position="top", fillcolor="pink", opacity=0.25, line_width=0.9, layer='below')

    for i, fig in enumerate(ref_figs):
        if i == 0:
            # set legend true
            fig.update_traces(showlegend=True)
        for trace in fig['data']:

            subfig.add_trace(trace, secondary_y=False, row=i+1, col=1)

    return subfig, dfs

def plot_rldc(data, ordered_tech_color, tech_map={}):
    ordered_list = list(ordered_tech_color.keys())
    if not tech_map:
        tech_map = {k: k for k in ordered_list}

    for key in ordered_list:
        if key not in tech_map:
            tech_map[key] = key

    cols = []
    color = []
    for item in ordered_list:
        if item in data.columns:
            cols.append(item)
            for k, v in ordered_tech_color.items():
                if k in item:
                    color.append(v)
                    break
        else:
            print(f"{item} is not in data.")

    framecols = [
        col for col in data.columns.to_list() if col not in ["rldc"]
    ]
    for col in framecols:
        if col not in cols:
            print(f"{col} data item is not in the selected headings")

    hidden_label = []
    hidden_label_clue_starts = ["Outflow:"]
    hidden_label_clue_ends = ":Outflow"

    for col in cols:
        for clue_start in hidden_label_clue_starts:
            if col.startswith(clue_start) & col.endswith(hidden_label_clue_ends):
                hidden_label.append(col)

    x = data.index.values
    y0 = data["rldc"].values
    y0_lab = "rldc"

    y1 = []
    y1_lab = []
    for col in cols:
        y1.append(data[col].values)
        if col in hidden_label:
            y1_lab.append("_")
        else:
            y1_lab.append(tech_map[col])

    fig = plt.figure(figsize=(18, 10))
    ax1 = fig.add_subplot(111)
    ax1.stackplot(x, y1, labels=y1_lab, colors=color)
    ax1.plot(x, y0, label=y0_lab, color="r")
    ax1.set_xlabel("hours")
    ax1.set_ylabel("Load [MW]")
    fig.legend(loc="lower left")
    return fig