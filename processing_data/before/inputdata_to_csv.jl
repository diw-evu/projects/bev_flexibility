

using GAMS
using JuMP
using DIETER
using DataFrames
using CSV

input_data_path =           joinpath(pwd(), "input_data")
result_data_path =          joinpath(pwd(), "input_csv")


println("Loading input data...")
@time dieter_data = DieterModel(input_data_path, dense_or_sparse=:sparse, verbose=true)

for (var_name,var_array) in dieter_data.parameters
    df = convert_jump_container_to_df(var_array, dim_names=get_dims(string(var_name))[2])
    CSV.write(joinpath(result_data_path, string(var_name)*".csv"), df)
end

for (var_name,var_array) in dieter_data.sets
    df = DataFrame(var_name => var_array)
    CSV.write(joinpath(result_data_path, string(var_name)*".csv"), df)
end