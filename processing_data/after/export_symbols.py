import os
import karray as ka
import symbolx as syx
from symbolx import DataCollection, SymbolsHandler, Symbol
from multiprocessing import Pool
from symlist import symbol_list

cores = 1

RawData_folder = "/home/cgaete/models/bev/results/"
symbols_folder = "/home/cgaete/models/bev/"

folder_zipped = False

SH = SymbolsHandler(method='folder', folder_path=symbols_folder)
token = SH.symbol_handler_token

# compare symbol_list with SH, and store not found
pending = []
for file_name, original_name, value_type in symbol_list:
    if (file_name, value_type) not in SH.symbols_book:
        pending.append((file_name, original_name, value_type))

print(f"Pending symbols: {len(pending)}")
for file_name, original_name, value_type in pending:
    print(file_name, original_name, value_type)

def file_name_func(folder_path:str, name:str, symbol_handler_token:str, value_type:str="v"):
    os.makedirs(folder_path, exist_ok=True)
    file_name = f"{name}.{value_type}.{symbol_handler_token}.feather"
    return os.path.join(folder_path,file_name)



DC = DataCollection()
DC.add_collector(collector_name='julia_model', parser=syx.symbol_parser_feather, loader=syx.load_feather)
DC.add_folder('julia_model', RawData_folder)
DC.add_custom_attr(collector_name='julia_model')
if folder_zipped:
    DC.adquire(id_integer=True, zip_extension='7z')
else:
    DC.adquire(id_integer=True, zip_extension=None)
SH = SymbolsHandler(method='object', obj=DC, symbol_handler_token=token)


ka.settings.order = ['id','n','gen','sto','ev','elyh2','stoh2','reconh2','l','h']
# ka.settings.sort_coords = True

def process_tuple(vars):
    file_name, original_name, value_type = vars
    symbol = Symbol(original_name, value_type=value_type, symbol_handler=SH)
    symbol.name = file_name
    symbol.to_feather(file_name_func(folder_path=symbols_folder,
                                    name=file_name,
                                    value_type=value_type,
                                    symbol_handler_token=SH.symbol_handler_token))
    return {original_name: {file_name: value_type}}

# Set the number of processes to use
cores_numbers = cores if len(pending) > os.cpu_count() else len(pending)

# Create a Pool with the number of processes equal to the number of available processors
pool = Pool(cores_numbers)

# Apply the `process_tuple` function to each tuple in parallel
results = pool.map(process_tuple, pending)

# Close and join the pool to ensure all tasks are completed
pool.close()
pool.join()

for value in results:
    print(value)