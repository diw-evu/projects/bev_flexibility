import os
import shutil
import glob
import pandas as pd

long_id_table_file = os.path.join('02iteration_data_inf', 'table_inf.csv')
long_id_list = pd.read_csv(long_id_table_file)['long_id'].tolist()

def find_folder_and_move_to_target_folder(long_id, source_folder, target_folder):
    for folder in glob.glob(os.path.join(source_folder, '*')):
        if os.path.isdir(folder) and long_id in folder:
            shutil.move(folder, target_folder)
            
            
for long_id in long_id_list:
    find_folder_and_move_to_target_folder(long_id, '/home/cgaete/models/bev_results/02results', '/home/cgaete/models/bev_results/inf')
    
