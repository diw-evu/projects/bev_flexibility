# bev_flexibility

## 04-10-2024

See reports

V3 is now the default version. Compared to previous versions, the capital costs for wind and photovoltaic (PV) systems have decreased.
V4 introduces a cap on charging and discharging rates. For each hour, the rate must not exceed 25% of the installed capacity. This limitation accounts for constraints in transmission and distribution systems.

See the new contraints here:
https://gitlab.com/diw-evu/dieter_public/DIETERjl/-/blob/Flexibility_BEV_research_v3/src/07_ev.jl?ref_type=tags#L312