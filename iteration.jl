using GAMS
using JuMP
using DIETER

using ArgParse

function parse_commandline()
    s = ArgParseSettings()

    @add_arg_table s begin
        "--hours", "-H"
            help = "Hours to consider to run the model from zero"
            arg_type = Int
            default = 336
        "--from", "-f"
            help = "Starting scenario number from 1 on"
            arg_type = Int
            default = 1
        "--to", "-t"
            help = "Last scenario number"
            arg_type = Int
            default = 1
        "--output", "-o"
            help = "Output folder name in results directory"
            arg_type = String
            default = "test"
        "--rng", "-r"
            help = "Range of scenarios to run separated by comma"
            arg_type = String
    end

    return parse_args(s)
end

function main()
    parsed_args = parse_commandline()
    println(parsed_args)
    # take starting time
    start_time = time()
    println("Starting iteration process...")

    input_data_path =           joinpath(pwd(), "model", "data", "input_data")
    iteration_table_file =      joinpath(pwd(), "model", "data", "iteration_data", "IterationTable.csv")
    iteration_timeseries_path = joinpath(pwd(), "model", "data", "iteration_data")
    result_data_path =          joinpath(pwd(), "results", parsed_args["output"])


    hours = parsed_args["hours"]
    if !isnothing(parsed_args["rng"])
        runs = [parse(Int, x) for x in split(parsed_args["rng"], ",")]
    else
        runs = collect(parsed_args["from"]:parsed_args["to"])
    end

    save_model = false
    selected_symbols = nothing
    results_compress = nothing
    results_sparse = true
    results_verbose = false


    println("Loading input data...")
    @time dieter_data = DieterModel(input_data_path, dense_or_sparse=:sparse, verbose=true)

    println("\nReading iteration table...")
    @time scenarios = iteration_table(iteration_table_file, dieter_data, iteration_timeseries_path)

    container = []
    for (idx, scen_nr) in enumerate(runs)
        println("\nBuilding scenario $scen_nr...")
        run, dtr, metadata = prepare_scenario(dieter_data, scenarios, scen_nr)
        @assert run == scen_nr "run $idx is not in the collection. See Iteration Table"
        model = Model()
        set_optimizer(model, GAMS.Optimizer)
        set_optimizer_attribute(model, GAMS.SysDir(), ENV["GAMS_DIR"])
        set_optimizer_attribute(model, GAMS.Solver(), "CPLEX")
        set_optimizer_attribute(model, GAMS.ModelType(), "LP")
        set_optimizer_attribute(model, GAMS.Threads(), 0)
        set_optimizer_attribute(model, "solutiontype", 2) # 0 = Auto, 1 = Basic, 2 = Non-basic (crossover off)
        set_optimizer_attribute(model, "lpmethod", 4)
        println("\nStarting optimization $scen_nr...")
        @time msg = scenario_optimization(run, model, dtr, metadata, hours, save_model, result_data_path, selected_symbols, results_compress, results_sparse, results_verbose)
        push!(container, msg)
    end


    println("\nSummary...\n")
    for msg in sort(container)
        println(msg)
    end

    println("\n\nTotal time: ", round(time() - start_time, digits=2), " seconds.")
    println("\n\nOptimization process finished!")
end


main()
